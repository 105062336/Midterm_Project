# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Shopping Website
* Key functions (add/delete)
    1. Product page
    2. Shopping pipeline
    3. User dashboard
    4. SignIn / Up
* Other functions (add/delete)
    1. Add To Cart
    2. About Page
    3. Contact Page
    4. Confirm Password
    5. Comment Under The Product
    6. ChatRoom
    7. Secret Chatroom

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
( All Photos In My Website Are From SUITANGTANG Official, And All Of Them Are For Education Purpose.<a href="https://www.suitangtang.com/">Suitangtang</a>)
* <a href="http://sc.chinaz.com/moban/170629314560.htm" style="font-size:36px;">Template</a>
* <a style="font-size:36px;">Home Page</a> 
    <img src="images/homepage.png">
* <a style="font-size:36px;">About</a>
    <img src="images/about.png">
* <a style="font-size:36px;">Women's Wear</a>
    <img src="images/wear.png">
* <a style="font-size:36px;">Contact</a>
    <img src="images/contact.png">
* <a style="font-size:36px;">Checkout</a>
    <img src="images/pay.png">
    * Only When You Have Logged In Can You Pay For Products
    * When YOu Have Already Paid For The Products, And There Will Be A Shopping List Below The Payment Page. ( Note : It Can Only Record The Lastest Record. )
        <img src="images/history.png">
* <a style="font-size:36px;">Cart Icon</a>
    <img src="images/cart.png">
    * Item You Just Pick Up Will Appear Here
* <a style="font-size:36px;">Above The Page</a> 
    * Sign In
        <img src="images/signin.png">
        * Email / Password
        * Google Signin
        * Facebook Signin
        * Forgot Password And We Can Recieve The Password Reset Email
        * Log out
            <img src="images/account.png"/>
    * Sign Up
        <img src="images/signup.png">
        * Name (Not Necessary)
        * Email 
        * Password
        * Confirm Password
    * Phone Number
    * Email Address
* <a style="font-size:36px;">Below The Page</a>
    * Information
        <img src="images/footer.png">

## Midterm Project Todo List
* <a style="font-size:36px;">Basic components (All In Website Detail Description)</a>
    * Membership Mechanism (20%)
    * Host on your GitLab page (5%)
    * Database read/write (15%)
    * RWD (15%)
    * Topic Key Functions (15%)
        * Shopping website
            * Product page
            * Shopping pipeline
            * User dashboard
* <a style="font-size:36px;">Advanced components</a>
    * Sign Up/In with Google or other third-party accounts (2.5%) (It Is Concludeed In The Website Detail Description As Well. )
    * Add Chrome notification (5%)
        <img src="images/askfornotification.png">
        <img src="images/notification2.png">
    * Use CSS animation (2.5%)
    * Proove your website has strong security (5%)
    * Other functions not included in key functions (1~10%)
        * Comment Under The Product
        * Chat With The People With Online Chatroom
        * You Can Exchange The Chatroom By Typing '@'+RoomCode (The Default One Is @public)
            <img src="images/at.png">
        * You Can Create New Private Room To Chat With Someone By Typing 'Create/S'+RoomCode.
            <img src="images/create.png">
        * To Get Into The Private Room, You Need To Type '*S'+RoomCode.
            <img src="images/pass.png">
            <img src="images/secretroom.png">
        * When Your Cursor Hovers On The Chat Content, It Will Appear The Sent Time Of Message.
        * When You Type The Product Name On The Message, The Page Will Load To Its Product Page.
        * Press ESC Or Click The Remaining Space Of Chat RoomTo Leave The Chat Room
        * Type 'cd..' You Can Back To @public ChatRoom


## Security Report (Optional)
* If You Do Not Login, You Cannot Read And Write On This Firebase. 

        {
            "rules": {
                ".read": "auth != null",
                ".write": "auth != null"
            }
        }
    