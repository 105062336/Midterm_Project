$(function () {
    var post_btn = document.getElementById('post_btn');
    var post_txt = document.getElementById('comment');
    var Today = new Date();
    var user = firebase.auth().currentUser;
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('com_list5').push();
            newpostref.set({
                email: firebase.auth().currentUser.displayName,
                data: post_txt.value,
                photo: firebase.auth().currentUser.photoURL,
                Year: Today.getFullYear(),
                Month: Today.getMonth()+1,
                Day: Today.getDate(),
                Hours: Today.getHours(),
                Minutes: Today.getMinutes(),
                Seconds: Today.getSeconds()
            });
            post_txt.value = "";
        }
    });
    $('#comment').on('keydown', function (e) {
        if (e.keyCode == 13) {
            writeData();
        }
    });
    function writeData() {
        if (post_txt.value != "") {
            var Renew = new Date();
            var newpostref = firebase.database().ref('com_list5').push();
            newpostref.set({
                email: firebase.auth().currentUser.displayName,
                data: post_txt.value,
                photo: firebase.auth().currentUser.photoURL,
                Year: Renew.getFullYear(),
                Month: Renew.getMonth() + 1,
                Day: Renew.getDate(),
                Hours: Renew.getHours(),
                Minutes: Renew.getMinutes(),
                Seconds: Renew.getSeconds()
            });
            post_txt.value = "";
        }
    }
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n<br>";
    var postsRef = firebase.database().ref('com_list5');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                var year = childSnapshot.val().Year;
                var month = childSnapshot.val().Month;
                var day = childSnapshot.val().Day;
                var hours = childSnapshot.val().Hours;
                var minutes = childSnapshot.val().Minutes;
                var seconds = childSnapshot.val().Seconds;
                if (hours < 10)
                    hours = '0' + hours;
                if (minutes < 10)
                    minutes = '0' + minutes;
                if (seconds < 10)
                    seconds = '0' + seconds;
                total_post[total_post.length] = "<img src=" + childData.photo + "class='mr-2 rounded' style='height:32px; width:32px;'>" + childData.email + "<div style='float:right;font-color:gray;'>" + year + " / " + month + " / " + day + " " + hours + ":" + minutes + ":" + seconds + "</div>" + "<div style='text-align:left;font-size:16px;padding:20px;border-color:black;border-style:solid;border-width:1px;border-radius: 5px;'>" + str_before_username + "</div>" + "</strong>" + childData.data + str_after_content;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var year = data.val().Year;
                    var month = data.val().Month;
                    var day = data.val().Day;
                    var hours = data.val().Hours;
                    var minutes = data.val().Minutes;
                    var seconds = data.val().Seconds;
                    if (hours < 10)
                        hours = '0' + hours;
                    if (minutes < 10)
                        minutes = '0' + minutes;
                    if (seconds < 10)
                        seconds = '0' + seconds;
                    total_post[total_post.length] = "<img src=" + childData.photo + "class='mr-2 rounded' style='height:32px; width:32px;'>" + childData.email + "<div style='float:right;font-color:gray;'>" + year + " / " + month + " / " + day + " " + hours + ":" + minutes + ":" + seconds + "</div>" + "<div style='text-align:left;font-size:16px;padding:20px;border-color:black;border-style:solid;border-width:1px;border-radius: 5px;'>" + str_before_username + "</div>" + "</strong>" + childData.data + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
});
